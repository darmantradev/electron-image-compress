const { ipcRenderer } = require('electron');
const fs = require('fs');
const path = require('path');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');

let outputDir = path.join(require('os').homedir(), 'Desktop', 'Compressed Images');
const outputDirEL = document.getElementById('output-dir');

//Allowed file types
const fileTypes = [
  'image/jpeg',
  'image/png',
  'image/svg+xml'
];

window.addEventListener('DOMContentLoaded', (event) => {
  setOutputDir(outputDir);
  renderEmptyTemplate();
});

//Set Output Dir
function setOutputDir( dirPath ) {
  outputDirEL.innerHTML = path.normalize( dirPath );
}

//Open directory dialog
const selectImage = document.getElementById('set-output-folder');
selectImage.addEventListener('click', () => {
  ipcRenderer.send('open-dir-dialog');
});

//Put files to list
ipcRenderer.on('selected-dir', function (event, dir) {
  if( dir.length ) {
    setOutputDir( dir[0] );
  }
});

//Compress Images
function compressImage( filesObj ) {
  let outputPath = outputDirEL.innerText.split(path.sep);
  if( filesObj.length ) {
    [...filesObj].forEach((item, i) => {

      if( !isFileTypeAllowed(item) ) {
        return;
      }

      let inputPath = item.path.split(path.sep);
      const files = imagemin([inputPath.join('/')], {
        destination: outputPath.join('/'),
        plugins: [
          imageminMozjpeg({
            quality: 75
          }),
          imageminPngquant({
            quality: [0.6, 0.8]
          }),
          imageminSvgo({
            plugins: [
              {removeViewBox: false}
            ]
          })
        ]
      }).then( (files) => {
        if( files.length ) {
          let destinationPath = files[0].destinationPath,
              name = path.basename(destinationPath);

          const currentEL = document.querySelector('tr[title="'+ name +'"]');

          let stats = getCompressedStats( currentEL.getAttribute('data-size'), destinationPath );

          currentEL.classList.add('complete');
          currentEL.querySelector('.status').innerHTML = '<i class="icon-check"></i>';
          currentEL.querySelector('.size-after').innerText = stats.sizeAfter;
          currentEL.querySelector('.percent-differ').innerText = stats.percent;
        }
      }).catch( (err) => {
        console.log( err );
      });
    });
  }
}

//Upload files
const dropzone = document.getElementById('dropzone')
const fileList = document.getElementById('filelist');
const upload = document.getElementById('upload');

upload.addEventListener('change', triggerCallback);

dropzone.addEventListener('dragover', (e) => {
  e.preventDefault();
  e.stopPropagation();
  dropzone.classList.add('dragover');
});

dropzone.addEventListener('dragleave', (e) => {
  e.preventDefault();
  e.stopPropagation();
  dropzone.classList.remove('dragover');
});

dropzone.addEventListener('drop', (e) => {
  e.preventDefault();
  e.stopPropagation();
  dropzone.classList.remove('dragover');
  triggerCallback(e);
});

function triggerCallback(e) {
  let files;
  if(e.dataTransfer) {
    files = e.dataTransfer.files;
  } else if(e.target) {
    files = e.target.files;
  }

  renderTemplate(files);
  compressImage(files);
}

function renderTemplate(files) {
  if( !files.length ) {
    return;
  }

  let template = '',
      status = '<img src="./assets/loader.svg">',
      statusClass = '',
      stringLen = 20,
      more = '...';

  [...files].forEach((file, i) => {

    if( !isFileTypeAllowed(file) ) {
      status = '<i class="icon-x"></i>';
      statusClass = 'not-allowed';
    }

    let fileName = path.basename(file.path, path.extname(file.path));

    if( fileName.length < stringLen ) {
      more = path.extname(file.path);
    }

    template += `
        <tr title="${file.name}" data-size="${file.size}">
          <td class="status">${status}</td>
          <td class="name">${fileName.substring(0, stringLen)}${more}</td>
          <td class="size-before">${formatFileSize(file.size)}</td>
          <td class="size-after"></td>
          <td class="percent-differ"></td>
        </tr>
    `
  });

  fileList.querySelector('tbody').insertAdjacentHTML('beforeend', template);
}

//Convert File Size
function formatFileSize(bytes,decimalPoint) {
   if(bytes == 0) return '0 Bytes';
   var k = 1024,
       dm = decimalPoint || 1,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

//Get metadata for compressed image
function getCompressedStats( beforeSize, filePath ) {
  let size = [],
      aferStats = fs.statSync(filePath),
      decrease = aferStats['size'] - beforeSize,
      percent = decrease / beforeSize * 100;

  size.sizeBefore = formatFileSize(beforeSize);
  size.sizeAfter = formatFileSize(aferStats['size']);
  size.percent = Math.round(percent) + '%';

  return size;
}

//Filter filetype
function isFileTypeAllowed( file ) {
  let status = false;

  if( fileTypes.includes(file.type) ) {
    status = true;
  }

  return status;
}

//Render empty list
function renderEmptyTemplate() {
  let template = `
  <tr class="empty">
    <td colspan="5"></td>
  </tr>
  `;

  fileList.querySelector('tbody').innerHTML = template;
}

//Clear all list
const clearAll = document.getElementById('clear-all');
clearAll.addEventListener('click', (e) => {
  e.preventDefault();
  renderEmptyTemplate();
});
